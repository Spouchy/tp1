package mickael.cianamea.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private ListView listView;
    final static String nomAnimalMessage = "TOTO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listAnimalView = findViewById(R.id.listView);;
        final String[] listAnimalString = AnimalList.getNameArray();

        ArrayAdapter<String> arrayAdapter =  new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listAnimalString);
        listAnimalView.setAdapter(arrayAdapter);

        this.listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, listAnimalString[position], Toast.LENGTH_SHORT).show();
                String nomAnimal = listAnimalString[position];
                Intent animalActivity = new Intent(MainActivity.this, AnimalActivity.class);
                animalActivity.putExtra(nomAnimalMessage, nomAnimal);
                startActivity(animalActivity);
            }
        });
    }
}
