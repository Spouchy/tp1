package mickael.cianamea.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent = getIntent();
        String nomAnimal = intent.getStringExtra(Main2Activity.RowHolder.nomAnimalMsg);
        //String nomAnimal = intent.getStringExtra(MainActivity.nomAnimalMessage);

        TextView nAnimal = findViewById(R.id.NomAnimal);
        nAnimal.setText(nomAnimal);

        final Animal animal = AnimalList.getAnimal(nomAnimal);

        ImageView imageAnimal = findViewById(R.id.ImageAnimal);
        int id = getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName());
        imageAnimal.setImageResource(id);

       TextView esperance = findViewById(R.id.EsperanceDeVie);
        esperance.setText(animal.getStrHightestLifespan());

        TextView poidNaissance = findViewById(R.id.PoidNaissance);
        poidNaissance.setText(animal.getStrBirthWeight());

        TextView poidAdulte = findViewById(R.id.PoidAdulte);
        poidAdulte.setText(animal.getStrAdultWeight());

        TextView gestation = findViewById(R.id.PeriodeGestation);
        gestation.setText(animal.getStrGestationPeriod());

        final TextView conservation = findViewById(R.id.StatutConservation);
        conservation.setText(animal.getConservationStatus());

        Button sauv = findViewById(R.id.Sauvegarde);
        sauv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                animal.setConservationStatus(conservation.getText().toString());
            }
        });

    }
}
