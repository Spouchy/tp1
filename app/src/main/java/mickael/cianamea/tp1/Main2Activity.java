package mickael.cianamea.tp1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    final String[] listAnimalString = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);


        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row, parent,false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder,int position) {
            Animal animal = AnimalList.getAnimal(listAnimalString[position]);
            int id = getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName());
            holder.bindModel(listAnimalString[position], id);
        }

        @Override
        public int getItemCount() {
            return(listAnimalString.length);
        }

    }

    public class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView label = null;
        ImageView icon = null;
        final static String nomAnimalMsg = "NomAnimal";


        RowHolder(View row) {
            super(row);
            label = row.findViewById(R.id.label);
            icon = row.findViewById(R.id.icon);
            row.setOnClickListener(this);

        }

        @Override
        public void onClick(View v){
            final String item =label.getText().toString();
            System.out.println(item);
            Toast.makeText(mickael.cianamea.tp1.Main2Activity.this, item, Toast.LENGTH_SHORT).show();
            Intent animalActivity = new Intent(mickael.cianamea.tp1.Main2Activity.this, AnimalActivity.class);
            animalActivity.putExtra(nomAnimalMsg, item);
            startActivity(animalActivity);

        }

        void bindModel(String item, int imageId) {
            label.setText(item);
            icon.setImageResource(imageId);
        }
    }
}
